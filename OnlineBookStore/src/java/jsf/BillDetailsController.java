package jsf;

import jpa.entities.BillDetails;
import jsf.util.JsfUtil;
import jsf.util.JsfUtil.PersistAction;
import jpa.session.BillDetailsFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("billDetailsController")
@SessionScoped
public class BillDetailsController implements Serializable {

    @EJB
    private jpa.session.BillDetailsFacade ejbFacade;
    private List<BillDetails> items = null;
    private BillDetails selected;

    public BillDetailsController() {
    }

    public BillDetails getSelected() {
        return selected;
    }

    public void setSelected(BillDetails selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
        selected.getBillDetailsPK().setBookId(selected.getBook().getBookId());
//        selected.getBillDetailsPK().setBillId(selected.getBookBill().getBillId());
    }

    protected void initializeEmbeddableKey() {
        selected.setBillDetailsPK(new jpa.entities.BillDetailsPK());
    }

    private BillDetailsFacade getFacade() {
        return ejbFacade;
    }

    public BillDetails prepareCreate() {
        selected = new BillDetails();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("BillDetailsCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("BillDetailsUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("BillDetailsDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<BillDetails> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public BillDetails getBillDetails(jpa.entities.BillDetailsPK id) {
        return getFacade().find(id);
    }

    public List<BillDetails> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<BillDetails> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = BillDetails.class)
    public static class BillDetailsControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            BillDetailsController controller = (BillDetailsController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "billDetailsController");
            return controller.getBillDetails(getKey(value));
        }

        jpa.entities.BillDetailsPK getKey(String value) {
            jpa.entities.BillDetailsPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new jpa.entities.BillDetailsPK();
//            key.setBillId(values[0]);
            key.setBookId(values[1]);
            return key;
        }

        String getStringKey(jpa.entities.BillDetailsPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getBillId());
            sb.append(SEPARATOR);
            sb.append(value.getBookId());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof BillDetails) {
                BillDetails o = (BillDetails) object;
                return getStringKey(o.getBillDetailsPK());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), BillDetails.class.getName()});
                return null;
            }
        }

    }

}
