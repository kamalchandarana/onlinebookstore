/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author kchandar
 */
@Embeddable
public class BillDetailsPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "BILL_ID")
    private BigInteger billId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "BOOK_ID")
    private String bookId;

    public BillDetailsPK() {
    }

    public BillDetailsPK(BigInteger billId, String bookId) {
        this.billId = billId;
        this.bookId = bookId;
    }

    public BigInteger getBillId() {
        return billId;
    }

    public void setBillId(BigInteger billId) {
        this.billId = billId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (billId != null ? billId.hashCode() : 0);
        hash += (bookId != null ? bookId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BillDetailsPK)) {
            return false;
        }
        BillDetailsPK other = (BillDetailsPK) object;
        if ((this.billId == null && other.billId != null) || (this.billId != null && !this.billId.equals(other.billId))) {
            return false;
        }
        if ((this.bookId == null && other.bookId != null) || (this.bookId != null && !this.bookId.equals(other.bookId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.BillDetailsPK[ billId=" + billId + ", bookId=" + bookId + " ]";
    }
    
}
